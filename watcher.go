// Copyright 2012 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// +build !plan9

package main

import (
	"fmt"
	"io/ioutil"
	"path/filepath"
	"strings"
	"time"

	"github.com/jackc/pgx"
	"github.com/pkg/errors"
	"github.com/rjeczalik/notify"
)

// once allows us to send files to apply immediately
var once = make(chan string, 1)

// watchFiles watches the paths + "current.sql" and functions directories
func watchFiles(conn *pgx.Conn, paths []string, pwd string) error {
	// Make the channel buffered to ensure no event is dropped. Notify will drop
	// an event if the receiver is not able to keep up the sending pace.
	c := make(chan notify.EventInfo, 1)

	defer notify.Stop(c)

	applyFile := func(path string) (done chan bool) {
		if strings.HasSuffix(path, ".sql") {
			start := time.Now()
			relPath, relErr := filepath.Rel(pwd, path)
			if relErr != nil {
				relPath = path
			}

			cm := migration{
				filename: path,
				relPath:  relPath,
			}

			err := readMigrationFile(&cm, path, false, false)
			if err != nil {
				Log(infoLevel, err)
				done <- true
			}

			_, err = validateSQL(cm.upSQL)
			var ct pgx.CommandTag
			var transactErr error
			if err != nil {
				Log(infoLevel, fmt.Sprintf("%s: %s", err, cm.filename))
			} else {
				ct, transactErr = transact(&cm, conn, false, false)
				// asFunction is false but migrate and commit update the schema
			}

			elapsed := time.Since(start).Truncate(time.Millisecond)

			Log(errorLevel, fmt.Sprintf("Applied %s in %s. Last CMD: %s", relPath, elapsed, ct))
			if transactErr != nil {
				Log(errorLevel, transactErr)
			}
		}
		return nil
	}

	done := make(chan bool)
	go func() {
		for {
			select {
			case event := <-c:
				path := event.Path()
				applyFile(path)
			case path := <-once:
				applyFile(path)
			}
		}
	}()

	// apply current.sql files
	go func() {
		for _, v := range paths {
			once <- filepath.Join(*root, v, "current.sql")
			if !config.DisableFunctions {
				fnFolder := filepath.Join(*root, v, "functions")
				// Read directory to get list of files to watch
				fis, err := ioutil.ReadDir(fnFolder)
				if err != nil {
					done <- true
				}
				for _, fn := range fis {
					once <- filepath.Join(fnFolder, fn.Name())
				}
			}
		}
	}()

	for _, path := range paths {
		// Set up a watchpoint listening on events within current working directory.
		// Dispatch each create and remove events separately to c.
		curr := filepath.Join(*root, path, "current.sql")
		Log(infoLevel, fmt.Sprint("Watching ", curr))
		if err := notify.Watch(curr, c, notify.Write); err != nil {
			return errors.Wrapf(err, "error watching %s", path)
		}
		if !config.DisableFunctions {
			fn := filepath.Join(*root, path, "functions")
			Log(infoLevel, fmt.Sprint("Watching ", fn))
			if err := notify.Watch(fn, c, notify.Write); err != nil {
				return errors.Wrapf(err, "error watching %s", fn)
			}
		}
	}
	<-done
	return nil
}

// validateSQL reads SQL and validates that SQL statements exist. Also returns hash and name.
func validateSQL(fileBytes []byte) (upSQL string, err error) {
	// Split file at comment for up and down sql
	upSQL = strings.TrimSpace(string(fileBytes))
	// Check to be sure there is SQL in forward migration step.
	containsSQL := false
	inComment := false
	for _, v := range strings.Split(upSQL, "\n") {
		// Ignore lines that start with comment characters
		cleanString := strings.TrimSpace(v)
		if len(cleanString) != 0 {
			if strings.HasPrefix(cleanString, "/*") {
				inComment = true
			}
			if !inComment && !strings.HasPrefix(cleanString, "--") {
				containsSQL = true
				break
			}
			if inComment && strings.HasSuffix(cleanString, "*/") {
				inComment = false
			}
		}
	}

	if !containsSQL {
		err = errors.New("no SQL statements found")
	}

	return
}
