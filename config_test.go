package main

import "testing"

var configTests = []string{"", `
---
project: Duck
databases:
  dev:
    dbname: duck_dev
    host: donald
    user: donald
    port: 3030
    has_password: true
    sslmode: preferred
  shadow:
    dbname: duck_shadow
`, `
databases:
  dev:
    host: localhost
    user: system
    dbname: medium
    has_password: true
    sslmode: disable
  prod:
    user: system
    dbname: medium
    has_password: true
    sslmode: disable
`, `
databases:
  dev:
    dbname: dev
  shadow:
    dbname: shadow
  prod:
    user: system
    dbname: prod
`, `
invalid yaml: key
	tabbed: true`, `
databases:
  dev:
    dbname: dev
  shadow:
    dbname: shadow
  prod:
    dbname:`, `
databases:
  dev:
	dbname: dev
	sslmode: bad
  shadow:
    dbname: shadow
`}

func TestLoadConfig(t *testing.T) {
	var tests = []struct {
		name        string
		shouldError bool
		input       string
		target      string
	}{
		{"Empty file", true, configTests[0], "dev"},
		{"Use default clean", false, configTests[1], "dev"},
		{"Missing Target", true, configTests[1], "notExists"},
		{"Missing shadow DB", true, configTests[2], "dev"},
		{"Includes all databases", false, configTests[3], "prod"},
		{"Invalid YAML fails", true, configTests[4], "dev"},
		{"Missing dbname fails", true, configTests[5], "dev"},
		{"Bad sslmode fails", true, configTests[6], "dev"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			input := []byte(tt.input)
			target = &tt.target
			config = Config{}
			err := loadConfig(&input)
			if tt.shouldError && err == nil {
				t.Errorf("wanted error for %s with target %s", tt.name, *target)
			}
			if !tt.shouldError && err != nil {
				t.Log(err)
				t.Errorf("should not error for %s. got: %v", tt.name, err)
			}
		})
	}
}
