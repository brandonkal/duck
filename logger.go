package main

import (
	"fmt"

	"github.com/jackc/pgx"
)

const errorLevel = 1
const infoLevel = 2
const debugLevel = 3

type L struct{}

// Log handles logging for pgx internals.
func (L) Log(level pgx.LogLevel, msg string, data map[string]interface{}) {
	fmt.Printf("log: %v msg: %s\n", level, msg)
}

// Log is a simple log handler for duck. It calls fmt.Println if it should be logged.
func Log(level int, msg interface{}) {
	if *logLevel >= level && msg != "" {
		fmt.Println(msg)
	}
}
