package main

import (
	"fmt"
	"os/user"

	"github.com/jackc/pgx"
	"github.com/pkg/errors"
	"gopkg.in/yaml.v2"
)

// Config Root
type Config struct {
	DB               map[string]*DB `yaml:"databases"`
	Project          string
	Placeholders     map[string]string
	DisableFunctions bool `yaml:"disable_functions,omitempty"`
}

var config Config

// DB is the database config
type DB struct {
	Host        string
	User        string
	Port        uint16
	DBName      string `yaml:"dbname"`
	HasPassword bool   `yaml:"has_password"`
	SslMode     string `yaml:"ssl_mode"`
	sslRootCert string `yaml:"ssl_root_cert"`

	// Private Implementation
	connConfig pgx.ConnConfig
}

// loadConfig loads and validates a config YAML, outputting the normalized config for all databases.
// If a target is specified, we verify that the target exists in the Config.Databases map.
func loadConfig(configBytes *[]byte) error {

	err := yaml.Unmarshal(*configBytes, &config)
	if err != nil {
		return errors.New("Failed to read YAML config file.")
	}

	if _, ok := config.DB["dev"]; !ok {
		return errors.New("A \"dev\" database must be defined in the config file.")
	}
	if _, ok := config.DB["shadow"]; !ok {
		return errors.New("A \"shadow\" database must be defined in the config file.")
	}
	if _, ok := config.DB[*target]; !ok {
		return errors.New(fmt.Sprintf("A \"%s\" database is not defined in the config file.", *target))
	}

	for k, db := range config.DB {
		// Validate dev database
		if db.DBName == "" {
			return errors.New("Databases config must contain dbname.")
		}

		if db.Host == "" {
			db.Host = "localhost"
			Log(debugLevel, fmt.Sprint("No host specified for database ", k, " assuming \"localhost\""))
		}

		if db.User == "" {
			user, err := user.Current()
			if err != nil {
				return errors.Wrap(err, "could not determine OS user")
			}
			db.User = user.Username
		}

		switch db.SslMode {
		case "", "disable", "allow", "prefer", "require", "verify-ca", "verify-full":
		default:
			return errors.New("ssl_mode is invalid. Must be one of: '',disable,allow,prefer,require,verify-ca,verify-full")
		}

		if db.Port == 0 {
			db.Port = 5432
			Log(debugLevel, fmt.Sprint("No port specified for ", k, " assuming 5432"))
		}

		// Finally, we must reference the important bits to the connConfig
		db.connConfig.Host = db.Host
		db.connConfig.Database = db.DBName
		db.connConfig.Port = db.Port
		if db.User != "system" {
			db.connConfig.User = db.User
		}
	}
	Log(debugLevel, "Config loaded")
	return nil
}
