package main

import "fmt"

const usagePrefix = `Usage: duck [OPTIONS] COMMAND

An elegant 🦆  database migration tool by Brandon Kalinowski.`

const usageCommands = `
Commands:
    migrate               Migrate the DB to the most recent version available
    watch                 Migrates latest changes and watches current and function SQL files
    commit [-m module]    Resets shadow DB, writes current migration to committed migrations,
                          migrates the shadow DB, and replaces the schema dump

    status                Displays database version and planned changes. A dry run of "migrate"

    init                  Initializes a new project folder structure with a config file
    login                 Logs in to the target database. Credentials are saved to the OS keychain.
    reset                 Resets the target database. Highly destructive!`

func usage() {
	fmt.Println(usagePrefix)
	flags.PrintDefaults()
	fmt.Println(usageCommands)
}

var configTemplate = []byte(`project: Duck
# Welcome to Duck migrations!
# The elegant database migration tool.
databases:
  dev:
    dbname: duck_dev
    # The fields below are optional:
    # host: duck.test
    # port: 5432
    # # set user == system to authenticate against the database using the OS username
    # user: system
    # has_password: true
    sslmode: preferred
  shadow:
    dbname: duck_shadow
  # A production database is required.
  # However, it doesn't have to be accessible from development machines unless -t prod is passed.
  # Typically, this is done within the CI environment after a duck commit on master.
  prod:
    dbname: duck_prod
    sslmode: verify-full
    has_password: true
# Function watching is enabled by default. This makes managing database functions elegant.
# If multiple developers share the same same dev database race conditions could occur.
# To avoid this issue, you may choose to disable functions:
# disable_functions: true
# Alternatively, you could specify an additional database i.e. "staging" and coordinate access.

# Placeholders can also be specified. Replacement is only supported for hook files.
# The :NAME_FORMAT is used here as this template format is also supported by psql.
# Placeholder names must be quoted to be valid yaml. Numbers should also be quoted as strings
# placeholders:
#   ':DATABASE_OWNER': donald
#   ':SECRET_NUMBER': '42'
`)

var currentTemplate = []byte(`/*
duck migration: initial
*/

-- DROP TABLE IF EXISTS app.users;
-- CREATE TABLE app.users (
--   id serial primary key,
--   first_name text not null
-- );
`)
