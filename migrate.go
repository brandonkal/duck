package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"sort"
	"time"

	"github.com/pkg/errors"

	"github.com/jackc/pgx"
	"gopkg.in/yaml.v2"
)

func migrate(conn *pgx.Conn, migr migrationSet, dryRun bool) error {
	Log(infoLevel, "Beginning migration")
	_, err := conn.Exec(`create schema if not exists duck;
	create table if not exists duck.migrations (
		id serial unique,
		module text not null,
		filename text not null unique,
		hash varchar(40) primary key,
		previous varchar(40) unique references duck.migrations,
		requires varchar(40) references duck.migrations,
		date timestamptz not null default now(),
		sql_hash varchar(40) not null
	);
	create table if not exists duck.functions (
		filename text primary key,
		hash varchar(40),
		date timestamptz not null default now()
	);
	`)
	if err != nil {
		return errors.Wrap(err, "error configuring duck version tables")
	}
	type dbQuery struct {
		id       int
		filename string
		fileHash string
		previous *string
		requires *string
		date     time.Time
	}
	var dbv dbQuery
	row := conn.QueryRow(`select id, filename, hash, previous, requires, date
	from duck.migrations order by id desc limit 1`)
	scanErr := row.Scan(&dbv.id, &dbv.filename, &dbv.fileHash, &dbv.previous, &dbv.requires, &dbv.date)
	if scanErr != nil && scanErr != pgx.ErrNoRows {
		return errors.Wrap(scanErr, "error determining current DB version")
	}
	// Null values are mapped to "NA" in the file meta
	na := "NA"
	if dbv.previous == nil {
		dbv.previous = &na
	}
	if dbv.requires == nil {
		dbv.requires = &na
	}

	// migrate
	var fileVer int
	if len(migr.keys) != 0 {
		fileVer = migr.keys[len(migr.keys)-1]
	}

	if dbv.id > fileVer {
		return fmt.Errorf("Database version is %d, which is greater than local version %d. Missing committed file(s)", dbv.id, fileVer)
	}

	dbMismatch := errors.New("The last migration applied to the database does not match the committed file.")
	changesRequired := false

	if dryRun {
		fmt.Println("Database version:  ", dbv.id, " 🦆")
		fmt.Println("Committed version: ", fileVer, " 🦆")
	}

	if fileVer > dbv.id {
		changesRequired = true
		if dryRun {
			fmt.Println("Undeployed changes:")
		} else {
			Log(errorLevel, fmt.Sprint("Migrating now: ", dbv.id, " 🦆  ---> ", fileVer))
		}
		// Validate local migrations
		if dbv.id != 0 {
			cm := migr.set[dbv.id]
			if cm.meta.ID != dbv.id {
				return dbMismatch
			}
			if cm.filename != dbv.filename {
				return dbMismatch
			}
			if cm.fileHash != dbv.fileHash {
				return dbMismatch
			}
			// we could stop here but we continue the checks to validate the DB row returned...
			if cm.meta.Previous != *dbv.previous {
				return dbMismatch
			}
			if cm.meta.Requires != *dbv.requires {
				return dbMismatch
			}
		}

		// First loop through all planned local migrations and validate them
		for i := dbv.id + 1; i <= fileVer; i++ {
			cm := migr.set[i]
			if cm == nil {
				return fmt.Errorf("Missing local migration %d", i)
			}
			lm := migr.set[i-1]
			if i == 1 && cm.meta.Previous != "NA" && cm.meta.Requires != "NA" {
				return fmt.Errorf("Invalid first migration %d. Requirements not fulfilled", i)
			} else if i != 1 && lm == nil {
				return fmt.Errorf("Unable to determine previous migration for %d", i)
			} else if i != 1 && cm.meta.Previous != lm.fileHash {
				return fmt.Errorf("Invalid previous migration for %d", i)
			}
		}

		Log(debugLevel, "Database migrations are valid")

		// Finally loop through all planned local migrations and and execute...

		if !dryRun {
			// Lock to ensure multiple migrations cannot occur simultaneously
			lockNum := int64(9628173550095224) // arbitrary random number
			Log(debugLevel, "Locking database for migration")
			if _, lockErr := conn.Exec("select pg_advisory_lock($1)", lockNum); lockErr != nil {
				return lockErr
			}
			defer func() {
				_, unlockErr := conn.Exec("select pg_advisory_unlock($1)", lockNum)
				Log(debugLevel, "Database unlocked")
				if err == nil && unlockErr != nil {
					err = unlockErr
				}
			}()
		}

		var id int
		for id = dbv.id + 1; id <= fileVer; id++ {
			cm := migr.set[id]
			if !dryRun {
				_, err := transact(cm, conn, true, false)
				if err != nil {
					return err
				}
				Log(errorLevel, fmt.Sprintf("migrated: %-30s  %-17s  [%s]", cm.meta.Name, cm.module, cm.filename))
			} else {
				fmt.Printf("  * %s            module=%s\n", cm.meta.Name, cm.module)
			}
		}
		// we are in an if block and the for loop completed, so we display this as the current version
		dbv.id = id - 1
	}

	if !config.DisableFunctions {
		// Find function folders
		Log(infoLevel, "Beginning function migration")
		for _, v := range wp {
			fnFolder := filepath.Join(*root, v, "functions")
			// Read directory to get list of files to watch
			fis, err := ioutil.ReadDir(fnFolder)
			if err != nil {
				return errors.Wrap(err, "unable to read a functions directory")
			}

			printedFunctionHeader := false

			for _, fn := range fis {
				path := filepath.Join(fnFolder, fn.Name())
				if filepath.Ext(path) == ".sql" {
					Log(debugLevel, fmt.Sprint("discovered function file: ", path))
					cm := migration{
						filename: path,
					}

					err = readMigrationFile(&cm, path, false, false)
					if err != nil {
						return errors.Wrapf(err, "could not read migration file %s", cm.filename)
					}

					_, err = validateSQL(cm.upSQL)
					if err != nil {
						Log(infoLevel, fmt.Sprint(err, path))
					} else {
						var matches bool
						err = conn.QueryRow(`SELECT EXISTS ( SELECT 1 FROM duck.functions WHERE
							filename = $1 AND hash = $2 )`, cm.filename, cm.fileHash).Scan(&matches)
						if err != nil {
							return errors.Wrapf(err, "failed checking function %s", cm.filename)
						}
						if !matches {
							if !dryRun {
								if _, err = transact(&cm, conn, false, true); err != nil {
									return err
								}
								Log(errorLevel, fmt.Sprintf("migrated function: %s", cm.filename))
							} else {
								if !printedFunctionHeader {
									fmt.Println("Outdated functions:")
									printedFunctionHeader = true
									changesRequired = true
								}
								fmt.Printf("  * %s\n", cm.filename)
							}
						}
					}
				}
			}
		}
	}

	if !changesRequired {
		Log(errorLevel, "Nothing to deploy (up-to-date)")
	} else if !dryRun {
		Log(errorLevel, fmt.Sprintf("Database migration complete. DB version: %d", dbv.id))
	}
	// Print function version
	if dryRun {
		var fnVer string
		hashErr := conn.QueryRow(`SELECT 'md5:' || md5(fn.agg) as hash FROM (
			SELECT string_agg(hash, ':' order by hash) as agg
			FROM duck."functions") as fn`).Scan(&fnVer)
		if hashErr != nil && hashErr != pgx.ErrNoRows {
			return errors.Wrap(scanErr, "error determining function hash")
		}
		fmt.Println("DB Function Version: ", fnVer)
	}
	return err
}

// transact runs a given migration. This enables sharing code between watcher and migrate.
// If record is true, the change will be recorded in the transaction.
// functions will always be recorded in the transaction, without regard to "record".
func transact(cm *migration, conn *pgx.Conn, record bool, asFunction bool) (pgx.CommandTag, error) {
	var tx *pgx.Tx
	var ct pgx.CommandTag
	var err error
	if !cm.meta.DisableTx {
		tx, err = conn.Begin()
		if err != nil {
			return ct, err
		}
		// nolint:errcheck
		defer tx.Rollback()
	}

	// Execute the migration
	Log(infoLevel, fmt.Sprint("Updating ", cm.filename))
	ct, err = conn.Exec(string(cm.upSQL))
	if err != nil {
		if err, ok := err.(pgx.PgError); ok && err.Position != 0 {
			var file string
			if cm.relPath != "" {
				file = cm.relPath
			} else {
				file = cm.filename
			}
			return ct, errors.Wrapf(err, "syntax error: %s", file)
		}
		return ct, err
	}

	if record && !asFunction {
		var previous *string
		var requires *string
		// We perform the conversion here so we can represent NULL as "NA" in YAML
		if cm.meta.Previous != "NA" {
			previous = &cm.meta.Previous
		}
		if cm.meta.Requires != "NA" {
			requires = &cm.meta.Requires
		}
		// Add one to the version
		_, err = conn.Exec(`insert into duck.migrations
		(id, module, filename, hash, previous, requires, sql_hash) values ($1, $2, $3, $4, $5, $6, $7)`,
			cm.meta.ID, cm.module, cm.filename, cm.fileHash, previous, requires, cm.meta.SQLHash)
		if err != nil {
			return ct, errors.Wrap(err, "error while updating DB version")
		}
	} else if asFunction {
		_, err = conn.Exec(`insert into duck.functions (filename, hash, date) values ($1, $2, NOW())
		on conflict (filename) do update set filename = $1, hash = $2, date = NOW()`,
			cm.filename, cm.fileHash)
		if err != nil {
			return ct, errors.Wrapf(err, "error while updating function %s version", cm.meta.Name)
		}
	}

	if !cm.meta.DisableTx {
		err = tx.Commit()
		if err != nil {
			return ct, errors.Wrapf(err, "error committing migration %s", cm.meta.Name)
		}
	}
	return ct, nil
}

type migrationSet struct {
	set  map[int]*migration
	keys []int
}

// loadAllMigrations loads a list of all committed migration files.
// It then reads each migration file and loads the metadata and file contents.
// It returns a migration map and a sorted slice of keys for the id numbers.
func loadAllMigrations() (migr migrationSet, err error) {
	// Check committed directory
	cFis, err := ioutil.ReadDir(filepath.Join(*root, "_committed"))
	if os.IsNotExist(err) {
		Log(debugLevel, "load migrations: No committed folder")
	}
	if err != nil {
		return migr, errors.Wrap(err, "loading migrations")
	}

	// We place migrations in a temporary slice before loading the metadata from disk.
	var migSlice []*migration
	for _, fi := range cFis {
		if fi.IsDir() {
			moduleName := fi.Name()
			mis, err := ioutil.ReadDir(filepath.Join(*root, "_committed", moduleName))
			if err != nil {
				err = errors.Wrap(err, "Unable to read a module directory")
				return migr, err
			}
			for _, mi := range mis {
				if filepath.Ext(mi.Name()) == ".sql" {
					migSlice = append(migSlice, &migration{
						filename: mi.Name(),
						module:   moduleName,
					})
				}
				Log(debugLevel, fmt.Sprintf("Migration file discovered: %s", mi.Name()))
			}
		}
	}

	migr.set = make(map[int]*migration)

	migr.keys = make([]int, 0, len(migSlice))

	for _, m := range migSlice {
		err := readMigrationFile(m, filepath.Join(*root, "_committed", m.module, m.filename), true, true)
		if err != nil {
			return migr, errors.Wrap(err, "could not read migration file")
		}
		// Finally we need a map
		migr.set[m.meta.ID] = m
		// https://blog.golang.org/go-maps-in-action#TOC_7.
		migr.keys = append(migr.keys, m.meta.ID)

	}
	sort.Ints(migr.keys)
	return migr, err
}

// readMigrationFile reads in a migration file for a given path. If validate is true, meta keys will be validated.
// A name meta key is always required.
func readMigrationFile(m *migration, path string, validate bool, reqName bool) error {
	data, err := ioutil.ReadFile(path)
	relPath := filepath.Join(m.module, m.filename)
	if err != nil {
		return errors.Wrapf(err, "Could not read file: %s", relPath)
	}
	m.upSQL = data
	m.fileHash = getHash(m.upSQL)

	// Load metadata
	metaSlice := bytes.SplitN(bytes.SplitN(m.upSQL, []byte("*/"), 2)[0], []byte("/*"), 2)
	if len(metaSlice) == 2 {
		meta := metaSlice[1]

		err := yaml.Unmarshal(meta, &m.meta)
		if err != nil {
			return errors.Wrapf(err, "error reading migration metadata: %s", path)
		}

		if validate {
			if m.meta.ID == 0 {
				return invalidErr(relPath)
			}
			if len(m.meta.Previous) != 40 && m.meta.Previous != "NA" {
				return invalidErr(relPath)
			}
			if len(m.meta.Requires) != 40 && m.meta.Requires != "NA" {
				return invalidErr(relPath)
			}
			if len(m.meta.SQLHash) != 40 {
				return invalidErr(relPath)
			}
		} else if reqName && m.meta.Name == "" {
			return fmt.Errorf("Invalid migration %s. A name is required.", relPath)
		}
	} else if validate {
		return invalidErr(relPath)
	}
	return nil
}
