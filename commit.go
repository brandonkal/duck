package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"

	"github.com/jackc/pgx"
	"github.com/pkg/errors"
	"gopkg.in/yaml.v2"
)

type step struct {
	isCommand bool
	contents  string
}

func (migr migrationSet) getLast(module string) *migration {
	var key int
	for _, k := range migr.keys {
		if migr.set[k] != nil && migr.set[k].module == module {
			key = k
		}
	}
	return migr.set[key]
}

// commit a specified migration (defaults to main module):
// 1. Parses commit CLI flags and validates placeholder strings.
// 2. Validates current.sql file. It must contain SQL and a name.
// 3. Builds the new migration with metadata block from the current.sql file.
// 4. Loads shadow database password and connects as admin.
// 5. Drops shadow database, recreates it and runs afterReset.sql hook.
// 6. Applies latest dump to shadow database
// 7. Writes the current migration to committed migrations.
// 8. Migrates the shadow database and replaces the dump.
// 9. Migrates the dev database.
// 10. Replaces current.sql file with template or deletes the committed file if the transaction failed.
func commit(conn *pgx.Conn, migr migrationSet) error {

	// parse module flag
	err := commitFlags.Parse(os.Args[2:])
	if err != nil {
		return err
	}

	// Validate placeholders before resetting a database
	if config.Placeholders == nil {
		config.Placeholders = make(map[string]string)
	} else {
		r, _ := regexp.Compile("^:[A-Z][A-Z0-9_]+$")
		for key, placeholder := range config.Placeholders {
			if !r.MatchString(key) || placeholder == "" {
				return fmt.Errorf("Invalid placeholder found: %s", key)
			}
		}
	}

	if _, ok := config.Placeholders[":DATABASE_OWNER"]; !ok {
		config.Placeholders[":DATABASE_OWNER"] = config.DB["shadow"].User
	}
	if _, ok := config.Placeholders[":DATABASE_NAME"]; !ok {
		config.Placeholders[":DATABASE_NAME"] = config.DB["shadow"].DBName
	}

	last := migr.getLast(*module)
	// Increment
	newMigration := migration{
		filename: "current.sql",
		module:   *module,
	}

	fullPath := filepath.Join(*root, *module, "current.sql")
	err = readMigrationFile(&newMigration, fullPath, false, true)
	if err != nil {
		return err
	}

	_, err = validateSQL(newMigration.upSQL)
	if err != nil {
		return errors.Wrap(err, fullPath)
	}

	// Calculate hash just for the SQL w/o metadata. This allows portability between project. i.e.
	// The previous and id keys can be updated while still allowing for update checks
	sqlBytes := bytes.SplitN(newMigration.upSQL, []byte("*/"), 2)[1]
	newMigration.meta.SQLHash = getHash(sqlBytes)

	// Fill in required content
	if len(migr.keys) > 0 {
		prevID := migr.keys[len(migr.keys)-1]
		newMigration.meta.ID = prevID + 1
		newMigration.meta.Previous = migr.set[prevID].fileHash
		if last != nil {
			newMigration.meta.Requires = last.fileHash
		} else {
			newMigration.meta.Requires = "NA"
		}
	} else {
		newMigration.meta.ID = 1
		newMigration.meta.Previous = "NA"
		newMigration.meta.Requires = "NA"
	}

	// Parse last file name
	var increment int64
	if last != nil && len(last.filename) > 0 {
		numT := strings.Split(last.filename, "__")
		num := numT[0][1:]
		increment, err = strconv.ParseInt(num, 0, 0)
		if err != nil {
			return fmt.Errorf("Unable to parse committed filename %s", last.filename)
		}
	}
	increment++
	migName := strings.Replace(strings.TrimSpace(newMigration.meta.Name), " ", "_", -1)
	if len(migName) > 30 {
		return errors.New("Migration names must be 30 characters or less")
	}
	newMigration.filename = fmt.Sprintf("%05d__%v.sql", increment, migName)

	// Marshal Result and write to file
	metaBlock, err := yaml.Marshal(&newMigration.meta)
	if err != nil {
		return errors.Wrap(err, "error parsing migration metadata")
	}

	// Prepare file to write to disk
	st := []byte("/*\n")
	ec := []byte("*/")

	nb := make([]byte, 0, len(st)+len(metaBlock)+len(sqlBytes))
	nb = append(nb, st...)
	nb = append(nb, metaBlock...)
	nb = append(nb, ec...)
	nb = append(nb, sqlBytes...)

	newMigration.fileHash = getHash(nb)

	err = os.MkdirAll(filepath.Join(*root, "_committed", *module), os.ModePerm)
	if err != nil {
		return err
	}

	// Initialize connection to shadow database
	config.DB["shadow"].loadPassword(false, false)

	// reset shadow DB
	Log(infoLevel, "Resetting shadow database...")
	shadow, err := config.DB["shadow"].reset(true)
	if err != nil {
		return err
	}

	fullPathCommitted := path.Join(*root, "_committed", *module, newMigration.filename)
	err = ioutil.WriteFile(fullPathCommitted, nb, 0644)
	if err != nil {
		return errors.Wrap(err, "writing file to committed migrations")
	}

	// reload migrations now that we have committed a new one
	freshMigrations, err := loadAllMigrations()
	if err != nil {
		os.Remove(fullPathCommitted)
		return err
	}

	err = migrate(shadow, freshMigrations, false)
	if err != nil {
		os.Remove(fullPathCommitted)
		return err
	}

	err = config.DB["shadow"].dump()
	if err != nil {
		os.Remove(fullPathCommitted)
		return err
	}

	err = migrate(conn, freshMigrations, false) // main "dev" database
	if err != nil {
		os.Remove(fullPathCommitted)
		return err
	}

	// Replace current.sql
	err = ioutil.WriteFile(path.Join(*root, *module, "current.sql"), []byte(template), 0644)
	if err != nil {
		return err
	}

	return nil
}

// reset resets a database. commit() uses reset() before migration. Pass useDump = false to reset only using migrations.
// Typically this is only done to the shadow database during a commit but it is also possible to manually run a reset.
// reset will open a new connection that it will close in order to drop the database.
// confNoDB := *config.DB["shadow"]
// It returns the connection (either adminConn or conn) for later use.
func (db *DB) reset(useDump bool) (*pgx.Conn, error) {
	confNoDB := *db
	confNoDB.connConfig.Database = ""
	adminConn, err := confNoDB.Connect()
	if err != nil {
		return adminConn, errors.Wrap(err, "connecting as admin")
	}
	Log(infoLevel, fmt.Sprintf("Dropping database: %s", db.DBName))

	_, err = adminConn.Exec(fmt.Sprintf("DROP DATABASE IF EXISTS %s", db.DBName))
	// This could fail if other users are connected to the shadow DB
	// It is not recommended to modify the shadow database directly as it is the source of truth.
	if err != nil {
		return adminConn, errors.Wrapf(err, "could not drop DB %s", db.DBName)
	}

	_, err = adminConn.Exec(fmt.Sprintf("CREATE DATABASE %s OWNER %s", db.DBName, db.User))
	if err != nil {
		return adminConn, errors.Wrapf(err, "creating DB %s", db.DBName)
	}
	_, err = adminConn.Exec(fmt.Sprintf("REVOKE ALL ON DATABASE %s FROM PUBLIC", db.DBName))
	if err != nil {
		return adminConn, err
	}
	Log(infoLevel, fmt.Sprintf("Database recreated: %s", db.DBName))

	// Reset connection
	err = adminConn.Close()
	if err != nil {
		return adminConn, errors.Wrap(err, "closing admin connection")
	}

	conn, err := db.Connect()
	if err != nil {
		return conn, errors.Wrap(err, "connecting to database")
	}

	if useDump {
		dump, err := ioutil.ReadFile(filepath.Join(*root, "dump.sql"))
		if os.IsNotExist(err) {
			Log(errorLevel, "No shadow dump file exists. Running all migrations...")
			err = nil
		} else if err == nil {
			Log(infoLevel, fmt.Sprintf("Applying database dump: %s", db.DBName))
			_, err = conn.Exec(string(dump))
		}
		if err != nil {
			return conn, errors.Wrap(err, "applying dump.sql to database")
		}
		Log(infoLevel, fmt.Sprintf("Database dump loaded: %s", db.DBName))
	}

	afterResetBytes, err := ioutil.ReadFile(filepath.Join(*root, "hooks", "afterReset.sql"))
	if !os.IsNotExist(err) && err != nil {
		Log(errorLevel, fmt.Sprintf("Unable to apply afterReset.sql. %s schema could be unhealthy", db.DBName))
	} else {
		afterReset := string(afterResetBytes)
		for key, placeholder := range config.Placeholders {
			afterReset = strings.Replace(afterReset, key, placeholder, -1)
		}
		steps := parseHookFile(afterReset)
		for i, step := range steps {
			if step.isCommand {
				Log(errorLevel, "Executing afterReset command")
				cmdOut, err := exec.Command("bash", "-c", step.contents).Output()
				if err != nil {
					fmt.Fprintln(os.Stderr, "A command called by afterReset hook returned an error: ", err)
					return conn, errors.Wrap(err, "afterReset hook cmd error")
				}
				fmt.Println(string(cmdOut))
			} else {
				ct, err := conn.Exec(step.contents)
				if err != nil {
					err = errors.Wrapf(err, "Error executing afterReset hook: step %d, last cmd: %s", i, ct)
					return conn, err
				}
			}
		}
	}
	Log(errorLevel, "Database reset complete")
	return conn, nil
}

// dump a database with pg_dump
func (d *DB) dump() error {
	var pwSuffix string
	s := d.connConfig
	if s.Password != "" {
		pwSuffix = ":" + s.Password
	}
	cs := "postgresql://" + s.User + pwSuffix + "@" + s.Host + ":" + strconv.Itoa(int(s.Port)) + "/" + s.Database
	cmd := exec.Command("pg_dump", "-s", cs)
	dumpFile, err := os.Create(filepath.Join(*root, "dump.sql"))
	if err != nil {
		return errors.Wrap(err, "creating dump file")
	}
	defer dumpFile.Close()
	cmd.Stdout = dumpFile
	cmd.Stderr = os.Stderr
	err = cmd.Start()
	if err != nil {
		fmt.Fprintln(os.Stderr, "pg_dump: ", err)
	}
	err = cmd.Wait()
	if err != nil {
		return errors.Wrap(err, "pg_dump failed")
	}
	Log(infoLevel, "Database dump updated")
	return nil
}

// parseHookFile takes the contents of a hook file (where shell commands are allowed, and splits into steps)
// It splits the string by the special comment line "--! command:".
// It then returns a list of steps, containing validated SQL and commands
func parseHookFile(contents string) []step {
	// Split into blocks starting with a command line. We then further split this up.
	// We split at "!-- com" and then test each block for "mand: "
	blocks := strings.Split(contents, "\n--! com")
	var steps []step

	for _, block := range blocks {
		if strings.HasPrefix(block, "mand: ") {
			// The first line of this block is a command statement
			trimmedBlock := strings.TrimPrefix(block, "mand: ")
			subSlice := strings.SplitN(trimmedBlock, "\n", 2)
			cmd := strings.TrimSpace(subSlice[0])
			if cmd != "" {
				steps = append(steps, step{
					isCommand: true,
					contents:  cmd,
				})
			}
			if len(subSlice) == 2 {
				upSQL, err := validateSQL([]byte(subSlice[1]))
				if err == nil {
					steps = append(steps, step{
						isCommand: false,
						contents:  upSQL,
					})
				}
			}
		} else {
			upSQL, err := validateSQL([]byte(block))
			if err == nil {
				steps = append(steps, step{
					isCommand: false,
					contents:  upSQL,
				})
			}
		}
	}
	return steps
}
