# Duck 🦆

Duck is an elegant database migrations and development tool for PostgreSQL.
Manage your database schema by creating SQL files and watch changes live!

![Duck in the water](https://images.ctfassets.net/2y6n0yr6o0ty/31jmA8Vq8wqgGyKoU4gECO/4cae394e449ec8c625ca592b63857253/10_Disneyland-JPG_074.jpg?h=360)

## Goals

- fast iteration speed -- save a file and the development database is updated in milliseconds
- roll-forward only -- you shouldn't need to maintain rollbacks. Production database should be linear.
- familiar -- just use SQL. No plan files or custom DSL.
- module support -- Postgres SQL only extensions are not an option with managed databases. Duck provides module support so your projects can share initial schema objects.
- integrated -- manage functions and standard migrations with the same tool
- secure -- if you connect to a password-protected database, Duck will use your OS keychain to store credentials (or a Docker secret). No environment variables or insecure files to leak access.

Duck was inspired by tools such as goose and sqitch. Initially I started with sqitch. This Perl tool aims to be as close as possible to git. Unfortunately, it also carries some baggage that you won't find in simpler migration tools like goose. This project aims to be the best of both worlds.

## Getting Started

`duck` requires two databases: the first is your main development database and the second is a "shadow" database which is used by the system to apply migrations and track schema changes. You should never interact with the "shadow" database directly. All team members should run the same PostgreSQL version to ensure the shadow dump matches for everyone. Docker and `brew services` are two ways of achieving this.

### `duck init`

Start your project with `duck init`. This will generate the required folder structure with a default "main" module, a config file, and a current.sql`

You can now edit the `duck.yml` file with the properties. Passwords aren't stored here, in order to use them, set `has_password: true` and `duck -t prod login`. Several databases are supported but the "dev" and "shadow" databases are required.

If you look at `db/main/current.sql` you will see:

```sql
/*
duck migration: initial
*/
```

You must specify a name here. When this file is committed, Duck will parse the comment block when it generates the filename. By default, all migrations are wrapped in a transaction. You can disable this like so:

```sql
/*
duck migration: vacuum blog_posts
disable transaction: true
*/
```

### `duck login`

Fetches the target database credentials from the keychain and attempts a database connection. If it fails, it will prompt for a password and save it to the OS keychain.

### `duck watch`

Watches files for changes. This is where the fun starts! `duck watch` will load any unapplied migrations and watch files for changes. For each module folder in `db/`, it will watch `current.sql` and all files in the `functions` directory. These files should be idempotent; i.e. running them multiple times should produce the same result.

### `duck commit`

This is where changes are locked in. These changes should be linear. `duck commit` should occur on the master branch, optionally within the CI environment.

- resets the shadow database
- applies `hooks/afterReset.sql` if it exists. This flexibility enables you to use other tools to manage other parts of the schema.
- applies the latest dump to the shadow database
- applies the current migration to the shadow database (via migrate)
- replaces the dump
- moves the current migration to committed migrations (adding a hash and metadata)
- replaces the current.sql file with the template for continuous development

The main module will be committed by default. So to commit `db/users/current.sql` instead, it is simply `duck commit -m users`.

### `duck migrate`

Brings the database up to date by running any un-executed committed migrations. It does _not_ run the `current.sql` files. This is safe for use in production.

The target defaults to the "dev" database in the config file. You could configure your CI to run `duck -t prod migrate` to migrate the "prod" database.

### `duck status`

This will print the current database version, the committed migrations version, a list of unapplied migrations, and a list of outdated functions. You can think of this as a dry run of `duck migrate`.

### `duck reset`

As the name suggests, this drops and recreates the database, running all committed migrations and functions from the start. Highly destructive. As a precaution, any database target containing "prod" cannot be reset in this fashion.

This command is useful if you break idempotency during development. Simply reset the development database.

## Functions

Functions (and views) are unique in that they can be dropped and recreated without fear of data loss.
Duck supports special handling of functions in addition to the standard linear migration workflow for schema management.
After every migration of committed files, Duck will scan function files for changes. New function files and functions whose hash differs from the hash currently recorded in the database are applied. Now you can `git diff` your database functions as they evolve in place!

Deleting function files will not remove them from the database. To remove functions, a standard migration should be used.

Function handling can be disabled in the `duck.yml`.

## Branching

Duck supports multiple developers working on different migrations in parallel. You can switch `git` branches. Idempotent migrations with `CASCADE` should make this possible without issue. `duck commit` must be linear. The commit could be done immediately before merging a branch to master or on master itself.

## Idempotency

`duck` is designed for fast iteration. You write database modifications in `db/main/current.sql` and every time you save it is run against the database, generally taking under 40ms.

Idempotency enables us to develop quickly (in real time). It also enables us to avoid writing separate down migrations. PostgreSQL has a number of idempotent commands:

```sql
drop table if exists ...
drop trigger if exists ...
create or replace function...
-- Change a function signature (arguments, return type, etc)
DROP FUNCTION IF EXISTS ... CASCADE;
CREATE OR REPLACE FUNCTION ...
```

When these are not an option, simply start a migration with the rollback. For example:

```sql
DROP TABLE IF EXISTS app.users;
CREATE TABLE app.users (
  id serial primary key,
  first_name text not null
);
```

When you commit your migration, `duck` will run the migration against the shadow database so you can be sure it is valid.

If you are working on multiple migrations in parallel, `DROP ... CASCADE` may be useful. Be sure to add back any dropped items once the entity is replaced. Reviewing the schema diff will help you spot these issues.

Here are some more examples of idempotent operations:

```sql
-- Change a function signature (arguments, return type, etc)
DROP FUNCTION IF EXISTS ... CASCADE;
CREATE OR REPLACE FUNCTION ...

DROP SCHEMA IF EXISTS app CASCADE;
CREATE SCHEMA app;


DROP TABLE IF EXISTS foo CASCADE;
CREATE TABLE foo ...;

-- Add a column to the end of the table
ALTER TABLE foo DROP COLUMN IF EXISTS bar CASCADE;
ALTER TABLE foo ADD COLUMN foo ...;

-- Make a column NOT NULL
ALTER TABLE foo ALTER COLUMN foo SET NOT NULL;

-- Alter a column type
ALTER TABLE foo ALTER COLUMN foo TYPE int USING foo::int;
```

## Configuration

Duck keeps things simple. Simply run `duck init` to get started. The generated config file will contain some helpful comments.

## Hooks

Duck will look for and execute a `db/hooks/afterReset.sql` file after performing a database reset. Note that Duck does not wrap statements in hook files in a transaction. Hook files support placehodler replacements and magic comments:

```sql
BEGIN;
GRANT ALL ON DATABASE :DATABASE_NAME TO :DATABASE_OWNER;

--! command: npx install-worker-tool-that-manages-its-own-schema

-- Require superuser privileges, so we run before migration time.
CREATE EXTENSION IF NOT EXISTS postgis;
COMMIT;
```

Note that the `--! command:` magic comment allows execution of an additional tool. Duck allows other tools to manage their own schema.

## Status

Duck is considered to be functionally complete. It is being used for database migrations with great success. There are currently no integration tests. Pull requests are welcome. If this project is useful to you, please consider reviewing the wish list below and opening a PR.

## Wish List

1. `duck import` import modules, updating their metadata to fit into the linear commit timeline. Optionally, modules could be downloaded from another git project or url.
2. `duck adopt` adopts an existing database to be managed by `duck`. Essentially, the existing schema dump would be Duck's first migration.
3. Library package. Duck is designed as a CLI tool first. But it could be useful as a library to embed in a Go app and run `migrate()` on startup.
4. Additional tests. E2E tests with [dockertest](https://github.com/ory/dockertest) and additional unit tests where practical.

A module system was built into Duck to make database schema development composable. Each module should manage its own objects, but it can still depend on objects it does not provide. For instance, a "blog posts" module may depend on a "users" table with an id column. Your "blog posts" module _could_ depend on your users module, but it doesn't really need to. All it cares about is that a users table exists with an id field. Sounds like Go interfaces doesn't it? Databases could be managed in this way.

In order to implement this, a `duck import` command would run a module's `verify.sql` file. This file would exist in the root of a module folder and is not allowed to modify the DB schema. If this file exists, `duck import` would execute the file and if the result of the query returns false or throws, the import would not be allowed. If users wanted explicit dependencies, the `verify.sql` could check for that: `SELECT version FROM migrate_version WHERE module = 'dependencyA'`. Otherwise, it could check only for what it needs, e.g. a utility function or a table with a specific column.

## Inspiration

In my search for an elegant database migration tool I came across sqitch (Perl), goose (Go), and graphile-migrate alpha (Node).
Duck is an attempt to learn from these tools and build something elegant. If Duck doesn't fit your needs, one of those tools may be useful.

## License

Source code licensed with The Parity Public License 6.0.0

Photo © 2010 Brandon Kalinowski
