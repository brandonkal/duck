package main

import (
	"crypto/sha1"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"

	"github.com/99designs/keyring"
	"github.com/jackc/pgx"
	"github.com/pkg/errors"
	"golang.org/x/crypto/ssh/terminal"
)

var VERSION = "0.1.0"

var (
	flags       = flag.NewFlagSet("duck", flag.ExitOnError)
	root        = flags.String("dir", "db", "root database directory with migration files")
	target      = flags.String("t", "dev", "specify target database. Shadow is managed automatically")
	logLevel    = flags.Int("v", 1, "log level must be 1-3 representing error,info,debug")
	help        = flags.Bool("h", false, "print help")
	version     = flags.Bool("version", false, "print version")
	commitFlags = flag.NewFlagSet("duck commit", flag.ExitOnError)
	module      = commitFlags.String("m", "main", "module to commit")
)

var template = `/*
duck migration:
*/


`

var wp []string // watch paths are used by the watcher and migrate to locate functions.

func invalidErr(name string) error {
	return errors.New(fmt.Sprintf("Invalid migration: %s. Has the file been tampered with?", name))
}

func main() {
	log.SetFlags(0)
	err := run()
	if err != nil {
		Log(errorLevel, err)
		os.Exit(1)
	}
}

// migration holds all the metadata for a given migration file.
type migration struct {
	filename string
	fileHash string
	relPath  string
	module   string
	upSQL    []byte
	meta     Meta
}

type Meta struct {
	Name      string `yaml:"duck migration"`
	ID        int
	Previous  string
	Requires  string
	SQLHash   string `yaml:"sqlHash"`
	DisableTx bool   `yaml:"disable transaction,omitempty"`
}

// run is the main logic. All non nil errors returned to main are logged as fatal
func run() error {
	flags.Usage = usage
	err := flags.Parse(os.Args[1:])
	if err != nil {
		return err
	}

	if *version {
		fmt.Println(VERSION)
		return nil
	}

	if *logLevel > 3 || *logLevel < 1 {
		flags.Usage()
		return nil
	}

	args := flags.Args()
	if len(args) == 0 || *help {
		flags.Usage()
		return nil
	}

	configPath := filepath.Join(*root, "duck.yml")

	// Connect to DB if required and load config file.
	var conn *pgx.Conn
	var migrations migrationSet
	if args[0] != "init" {

		migrations, err = loadAllMigrations()
		if err != nil {
			return err
		}

		// Read directory to get list of files to watch
		fis, err := ioutil.ReadDir(*root)
		if err != nil {
			return errors.Wrap(err, "Failed to read root directory")
		}

		for _, fi := range fis {
			if fi.IsDir() && fi.Name() != "_committed" && fi.Name() != "hooks" {
				wp = append(wp, fi.Name())
			}
		}

		configBytes, err := ioutil.ReadFile(configPath)
		if err != nil {
			return errors.Wrap(err, "Could not read config file")
		}
		err = loadConfig(&configBytes)
		if err != nil {
			return errors.Wrap(err, "config file parsing error")
		}

		config.DB[*target].loadPassword(false, false)

		// Connect
		if args[0] != "reset" && args[0] != "login" {
			conn, err = config.DB[*target].Connect()
			if err != nil {
				return errors.Wrapf(err, "Error connecting to %s database", *target)
			}
		}
	}

	switch args[0] {
	case "watch":
		pwd, err := os.Getwd()
		if err != nil {
			return err
		}
		// Migrate before watch
		err = migrate(conn, migrations, false)
		if err != nil {
			return err
		}
		return watchFiles(conn, wp, pwd)

	case "init":
		err := os.MkdirAll(filepath.Join(*root, "main", "functions"), os.ModePerm)
		if err != nil {
			return errors.Wrap(err, "init error")
		}
		err = makeNewFile(configPath, configTemplate)
		if err != nil {
			return errors.Wrap(err, "init error")
		}
		currentPath := filepath.Join(*root, "main", "current.sql")
		err = makeNewFile(currentPath, currentTemplate)
		if err != nil {
			return errors.Wrap(err, "init error")
		}
		err = os.MkdirAll(filepath.Join(*root, "_committed"), os.ModePerm)
		if err != nil {
			return errors.Wrap(err, "init error")
		}
		fmt.Println("Dnck project initialized")
		return err
	case "commit":
		return commit(conn, migrations)
	case "migrate":
		return migrate(conn, migrations, false)
	case "status":
		return migrate(conn, migrations, true)
	case "login":
		// Try connecting with existing password
		config.DB[*target].loadPassword(false, true)
		_, err := config.DB[*target].Connect()
		if err != nil && !strings.Contains(err.Error(), "28P01") {
			return err
		}
		// 28P01 == invalid password error code
		for i := 0; err != nil && strings.Contains(err.Error(), "28P01") && i < 3; i++ {
			if i > 1 {
				Log(errorLevel, "Invalid Password")
			}
			config.DB[*target].loadPassword(true, true)
			_, err = config.DB[*target].Connect()
		}
		if err == nil {
			Log(errorLevel, "Password authentication successful")
		}
		return err
	case "reset":
		if strings.Contains(*target, "prod") {
			return errors.New("Reset is not allowed on production database. Please reset manually.")
		}
		dbConn, err := config.DB[*target].reset(false)
		if err != nil {
			return errors.Wrap(err, "error resetting database")
		}
		return migrate(dbConn, migrations, false)

	default:
		flags.Usage()
		return nil
	}
}

func getHash(b []byte) string {
	return fmt.Sprintf("%x", sha1.Sum(b))
}

func (c *DB) Connect() (*pgx.Conn, error) {
	// If sslmode was set in config file or cli argument, set it in the
	// environment so we can use pgx.ParseEnvLibpq to use pgx's built-in
	// functionality.
	switch c.SslMode {
	case "disable", "allow", "prefer", "require", "verify-ca", "verify-full":
		if err := os.Setenv("PGHOST", c.connConfig.Host); err != nil {
			return nil, err
		}
		if err := os.Setenv("PGSSLMODE", c.SslMode); err != nil {
			return nil, err
		}
		if err := os.Setenv("PGSSLROOTCERT", c.sslRootCert); err != nil {
			return nil, err
		}

		if cc, err := pgx.ParseEnvLibpq(); err == nil {
			c.connConfig.TLSConfig = cc.TLSConfig
			c.connConfig.UseFallbackTLS = cc.UseFallbackTLS
			c.connConfig.FallbackTLSConfig = cc.FallbackTLSConfig
		} else {
			return nil, err
		}
	}

	if *logLevel == debugLevel {
		c.connConfig.LogLevel = pgx.LogLevelInfo
		c.connConfig.Logger = L{}
	}

	return pgx.Connect(c.connConfig)
}

func makeNewFile(filepath string, b []byte) error {
	_, err := os.Stat(filepath)
	if err != nil {
		if os.IsNotExist(err) {
			file, err := os.OpenFile(
				filepath,
				os.O_WRONLY|os.O_TRUNC|os.O_CREATE,
				0666,
			)
			if err != nil {
				fmt.Println("cannot open file")
				return err
			}
			defer file.Close()

			// Write bytes to file
			_, err = file.Write(b)
			return err
		} else {
			return err
		}
	}
	return errors.New(fmt.Sprintf("config file %s already exists", filepath))
}

func passwordPrompt(account string) (string, error) {
	fmt.Printf("Enter password for %s: ", account)
	b, err := terminal.ReadPassword(int(os.Stdin.Fd()))
	fmt.Println()
	if err != nil {
		return "", errors.Wrapf(err, "unable to read password for %s", account)
	}
	return string(b), nil
}

// loadPassword fetches the password from the OS keyring and adds it to the DB struct for connection.
// If reset is true or password is empty, it will save a new password in the keyring.
// If a SECRETS_FOLDER envar is set, the password will be loaded from the {{dbname}}-db-password file.
// The primary use case for this envar is to support Docker secrets.
func (c *DB) loadPassword(reset bool, interactive bool) {
	// Retrieve password if saved
	if c.HasPassword {
		// A secret can be retrieved from a file. This is useful for supporting Docker secrets.
		if sf := os.Getenv("SECRETS_FOLDER"); sf != "" {
			secretBytes, err := ioutil.ReadFile(filepath.Join(sf, c.DBName+"-db-password"))
			if err != nil {
				Log(errorLevel, err)
				os.Exit(1)
			}
			c.connConfig.Password = strings.TrimSpace(string(secretBytes))
		} else {
			// Open keyring
			kr, err := keyring.Open(keyring.Config{
				ServiceName:  "DuckDB",
				KeychainName: "login",
			})
			if err != nil {
				Log(errorLevel, err)
				os.Exit(1)
			}
			// Get password
			pass, err := kr.Get(c.DBName)
			if reset || err != nil || string(pass.Data) == "" {
				if !interactive {
					Log(errorLevel, errors.Wrapf(err, "password error for %s. Use \"duck login\"", c.DBName))
					os.Exit(1)
				}
				// Ask for password and save
				password, err := passwordPrompt(c.DBName)
				if err != nil {
					Log(errorLevel, err)
					os.Exit(1)
				}
				err = kr.Set(keyring.Item{
					Key:         c.DBName,
					Data:        []byte(password),
					Label:       "DuckDB",
					Description: "database password",
				})
				if err != nil {
					Log(errorLevel, errors.Wrap(err, "password error"))
					os.Exit(1)
				}
				c.connConfig.Password = password
			} else {
				c.connConfig.Password = string(pass.Data)
			}
		}
	}
}
