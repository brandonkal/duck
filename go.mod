module gitlab.com/brandonkal/duck

go 1.23

require (
	github.com/99designs/keyring v1.2.2
	github.com/jackc/pgx v3.6.2+incompatible
	github.com/pkg/errors v0.9.1
	github.com/rjeczalik/notify v0.9.3
	golang.org/x/crypto v0.31.0
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/99designs/go-keychain v0.0.0-20191008050251-8e49817e8af4 // indirect
	github.com/cockroachdb/apd v1.1.0 // indirect
	github.com/danieljoos/wincred v1.2.2 // indirect
	github.com/dvsekhvalnov/jose2go v1.8.0 // indirect
	github.com/godbus/dbus v4.1.0+incompatible // indirect
	github.com/gofrs/uuid v4.4.0+incompatible // indirect
	github.com/gsterjov/go-libsecret v0.0.0-20161001094733-a6f4afe4910c // indirect
	github.com/jackc/fake v0.0.0-20150926172116-812a484cc733 // indirect
	github.com/lib/pq v1.10.9 // indirect
	github.com/mtibben/percent v0.2.1 // indirect
	github.com/shopspring/decimal v1.4.0 // indirect
	golang.org/x/sys v0.28.0 // indirect
	golang.org/x/term v0.27.0 // indirect
	golang.org/x/text v0.21.0 // indirect
)
